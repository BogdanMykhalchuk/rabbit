package com.bgroup.rabbit;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static java.util.Objects.nonNull;

public class MessageConsumer implements Runnable {

    private final static String QUEUE_NAME = "hello";

    private String name;

    public MessageConsumer(String name) {
        this.name = name;
    }

    public void receiveMessage() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setPassword("skryvdoc");
        factory.setUsername("skryv");

        Connection connection = null;
        Channel channel = null;

        try {
            connection = factory.newConnection();
            channel = connection.createChannel();

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            QueueingConsumer consumer = new QueueingConsumer(channel);
            channel.basicConsume(QUEUE_NAME, true, consumer);

            // Process deliveries
            int i = 0;
            while (i < 5) {
                QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                /// here you are blocked, waiting the next message.
                String message = new String(delivery.getBody());
                System.out.println(name + " Received " + message);
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (nonNull(channel)) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }

            if (nonNull(connection)) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void run() {
        receiveMessage();
    }
}
