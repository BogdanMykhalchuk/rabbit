package com.bgroup.rabbit;

import static java.lang.Thread.sleep;

public class ApplicationStarter {

    private final static String QUEUE_NAME = "hello";


    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new MessageProducer());
        Thread thread2 = new Thread(new MessageConsumer("First consumer"));
        Thread thread3 = new Thread(new MessageConsumer("Second consumer"));

//        thread1.start();
//        sleep(1000);
//        thread2.start();
//        thread3.start();

        thread3.start();
        thread2.start();
        sleep(1000);
        thread1.start();
    }
}
